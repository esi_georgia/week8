package demo.services;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.PlantResource;

@RestController
@RequestMapping("/rest/plants")
public class PlantController {
	
	static List<PlantResource> plants;
	
	static String RentitURI = "http://rentit.com/rest/plants/";
	
	static {
		plants = new LinkedList<>();
		plants.add(createPlantResource(10001l, "Excavator", "1.5 Tonne Mini excavator", 150f));
		plants.add(createPlantResource(10002l, "Excavator", "3 Tonne Mini excavator", 200f));
		plants.add(createPlantResource(10003l, "Excavator", "5 Tonne Midi excavator", 250f));
		plants.add(createPlantResource(10004l, "Excavator", "8 Tonne Midi excavator", 300f));
		plants.add(createPlantResource(10005l, "Excavator", "15 Tonne Large excavator", 400f));
		plants.add(createPlantResource(10006l, "Excavator", "20 Tonne Large excavator", 450f));
	}

	static PlantResource createPlantResource(Long id, String name, String desc, Float price) {
		PlantResource plant = new PlantResource();
		plant.add(new Link(RentitURI + 10001));
		plant.setName(name);
		plant.setDescription(desc);
		plant.setPrice(price);
		return plant;
	}
		
	@RequestMapping(method=RequestMethod.GET, value="")
	public List<PlantResource> queryPlantCatalog(@RequestParam(value="name", required=false) String name) {
		
		System.out.println(">>>> Parameter: " + name);
		
		return plants;
	}
}
